import type { GetServerSideProps, NextPage } from "next";

import Item from "core/components/Item";
import Table from "core/components/Table";

import styles from 'styles/pages/tablePage.module.scss'
import { useRouter } from "next/router";
import { useEffect, useMemo, useState } from "react";

interface IItem {
  username: string;
  score: number;
  time: string;
}

interface IURL {
  name: string;
  value: string;
  title: string;
}

interface IProps {
  urls: IURL[];
}

const Home: NextPage<IProps> = ({
  urls
}) => {
  const router = useRouter();
  const [items, setItems] = useState<IItem[]>([]);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    setLoading(true);
    const endpointURL = urls.find(item => item.name == router.query.source)?.value;
    if(endpointURL){
      fetch(endpointURL)
        .then(res=>res.json())
        .then(data=>{
          const users = data.result.map((item: any) => {

            const minutes = Math.floor(item.time / 60)
            const secs = item.time - (minutes * 60);
      
            const minutes_rend = ("0" + minutes).slice(-2);
            const secs_rend = ("0" + secs).slice(-2);
      
            return {
              username: item.nickname || "-",
              score: item.score || "-",
              time: item.time ? `${minutes_rend}:${secs_rend}` : "-"
            };
          })

          setItems(users || []);
          setLoading(false);
        })
      }
  }, [urls, router.query]);

  const gameTitle = useMemo(()=>{
    return urls.find(item => item.name == router.query.source)?.title;
  }, [urls, router.query])

  return (
    <div>
      <main className={styles.page}>
        <h1 className={styles.page__title}>{gameTitle}</h1>
        {loading && (
          <div className="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        )}
        <Table
          limit={20}
        >
          {!loading && items.map((item, index) => (
            <Item {...item} key={index} />
          ))}
        </Table>
      </main>
    </div>
  );
};

export async function getStaticProps() {
  return {
    props: {
      urls: [
        {
          title: 'Match cards',
          name: 'game1',
          value: process.env.ENDPOINT_GAME_1
        },
        {
          title: 'Build the Logo',
          name: 'game2',
          value: process.env.ENDPOINT_GAME_2
        }
      ]
    }
  }
}

export default Home;
