const isProd = process.env.NODE_ENV === 'production'
const withImages = require('next-images')

module.exports = withImages({
  assetPrefix: isProd ? '/iktva-leaderboard-logo/' : '',
  trailingSlash: true,
});