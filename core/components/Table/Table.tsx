import React, { CSSProperties } from "react"

import styles from './Table.module.scss';

interface IProps {
  limit: number;
}

const Table: React.FC<IProps> = ({
  children,
  limit = 20
})=>{

  if(!Array.isArray(children)) {
    return <></>;
  }

  return <div
    className={styles.table}
    style={{
      '--max-in-col': Math.ceil(Math.min(limit, children.length) / 2)
    } as CSSProperties}
  >
    {children.filter((_, index) => index < limit)}
  </div>
}

export default Table;