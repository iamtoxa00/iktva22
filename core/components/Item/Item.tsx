import React from "react"
import styles from './Item.module.scss';

interface IProps {
  username: string;
  score: number;
  time: string;
}

const Item: React.FC<IProps> = ({
  children,
  score,
  time,
  username
}) => {
  return <div className={styles.item}>
    <div className={styles.item__container}>
      <span className={styles.item__title}>{username}</span>
      <div className={styles.item__score}>
        <img className={styles.item__icon} src={require('../../../public/images/star.svg')} alt=""/>
        <span className={styles.item__scoreLabel}>{score}</span>
      </div>
      <span className={styles.item__time}>{time}</span>
    </div>
  </div>
}

export default Item;